/* © Copyright 2020 Gareth Anthony Hulse
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "core.hpp"

namespace app
{
  class gpio: protected core
  {
  private:
    const unsigned int m_base = 0x10012000;
    volatile int
    *m_input = reinterpret_cast<int*> (m_base + 0x4),
      *m_output = reinterpret_cast<int*> (m_base + 0x8),
      *m_power = reinterpret_cast<int*> (m_base + 0xc),
      *m_input_pullup = reinterpret_cast<int*> (m_base + 0x10),
      *m_drive_strength = reinterpret_cast<int*> (m_base + 0x14),
      *m_iof_en = reinterpret_cast<int*> (m_base + 0x38),
      *m_iof_sel = reinterpret_cast<int*> (m_base + 0x3c);
    
    
  public:
    void
    output (const int &bits)
    {      
      write_address (m_output, bits);
    }
    
    void
    power (const int &bits)
    {
      write_address (m_power, bits);
    }

    void
    iof_enable (const int &bits)
    {
      write_address (m_iof_en, bits);
    }

    void
    iof_select (const int &bits)
    {
      write_address (m_iof_sel, bits);
    }

    void drive_strength (const int &bits)
    {
      write_address (m_drive_strength, bits);
    }
  };
}

   

