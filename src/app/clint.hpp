/* © Copyright 2020 Gareth Anthony Hulse
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "core.hpp"

#include <cstdint>

namespace app
{ 
  class clint : protected core
  {
  private:
    const int m_base = 0x2000000;
    volatile uint64_t
    *m_msip = reinterpret_cast<volatile uint64_t*> (m_base),
    *m_mtime = reinterpret_cast<volatile uint64_t*> (m_base + 0xbff8),
      *m_mtime_cmp = reinterpret_cast<volatile uint64_t*> (m_base + 0x4000);
    
  public:
    const static int clock = 32768;
    
    uint64_t
    mtime () const
    {
      return *m_mtime;
    }

    template <typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type>
    void
    mtime_cmp (const T &data)
    {
      write_csr (mie, 0 << 7);
      *m_mtime_cmp = data;
      write_csr (mie, 1 << 7);
    }
  };
}
