/* © Copyright 2020 Gareth Anthony Hulse
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "app/prci.hpp"
#include "app/util.hpp"
#include "app/clint.hpp"

#include <matilda/cpu/sifive/e31.hpp>

#include <cstdio>

namespace
{
  namespace cpu = matilda::cpu::sifive::e31;
  
  cpu::gpio gpio;
  cpu::pwm pwm_2 (2);
  app::clint clint;
  auto previous_time = clint.mtime ();
  uint64_t time_trigger_interval = app::util::seconds (2U);

  uint16_t pwm_values[] =
    {
      19181, // 1.7ms
      22378, // 1.3ms
      65535 // Stop
    };

  enum lcd_pins : uint32_t
    {
      rs = 1 << 13, // 19
      db7 = 1 << 12, // 18
      db6 = 1 << 11, // 17
      db5 = 1 << 10, // 16
      db4 = 1 << 3, // 11
      db3 = 1 << 21, // 5
      db2 = 1 << 22, // 6
      db1 = 1 << 23, // 7
      db0 = 1 << 4, // 12
      en = 1 << 2 // 10
    };

  void
  setup_prci ()
  {
    app::prci prci;

    prci.hfrosc_cfg (app::prci::bitmask::hfrosc_cfg::enable);

    prci.pll_cfg (app::prci::bitmask::pll_cfg::reference_select |
		  app::prci::bitmask::pll_cfg::bypass |
		  app::prci::bitmask::pll_cfg::select);

    prci.hfrosc_cfg (app::prci::bitmask::hfrosc_cfg::disable);
  }

  void
  setup_pwm ()
  {
    pwm_2.cfg (cpu::pwm::bitmask::cfg::enable_always |
	       cpu::pwm::bitmask::cfg::cmp_1_center |
	       cpu::pwm::bitmask::cfg::sticky |
	       cpu::pwm::bitmask::cfg::deglitch,
	       0);

    gpio.iof_en (1 << 11);
    gpio.iof_sel (1 << 11);
  }
}

int
main ()
{ 
  setup_prci ();

  /*
  //Motor example:
  pwm_2.cmp_1 (pwm_values[0]);
  clint.mtime_cmp (time_trigger_interval + clint.mtime () - previous_time);
  setup_pwm ();

  uint8_t i = 1;
  
  while (1)
    {
      app::core::wait_for_interrupt ();
      
      if (time_trigger_interval <= clint.mtime () - previous_time)
	{
	  previous_time = clint.mtime ();
	  clint.mtime_cmp (time_trigger_interval + clint.mtime () - previous_time);
	  
	  pwm_2.cmp_1 (pwm_values[i]);
	  
	  if (i < 2)
	    {
	      ++i;
	    }     
	  else
	    {
	      i = 0;
	    }      
	}
    }
  */
  
  //gpio.output_val (1 << 23 | 1 << 22 | 1 << 21 | 1 << 2);

  // LCD Example:
  gpio.output_en (lcd_pins::rs | lcd_pins::db7 | lcd_pins::db6 | lcd_pins::db5 | lcd_pins::db4 | lcd_pins::db3 | lcd_pins::db2 | lcd_pins::db1 | lcd_pins::db0 | lcd_pins::en);
  
  // Turn on display:
  gpio.output_val (lcd_pins::db3 | lcd_pins::db2 | lcd_pins::en);
  app::util::delay (app::util::miliseconds (7U));
  gpio.output_val (-(lcd_pins::db3 | lcd_pins::db2 | lcd_pins::en));

  app::util::delay (app::util::miliseconds (7U));

  // Clear screen:
  gpio.output_val (lcd_pins::db0 | lcd_pins::en);
  app::util::delay (app::util::miliseconds (7U));
  gpio.output_val (-(lcd_pins::db0 | lcd_pins::en));

  app::util::delay (app::util::miliseconds (7U));

  //ASCII Characters:
  // H:
  gpio.output_val (lcd_pins::rs | lcd_pins::db6 | lcd_pins::db3 | lcd_pins::en);
  app::util::delay (app::util::miliseconds (7U));
  gpio.output_val (-(lcd_pins::rs | lcd_pins::db6 | lcd_pins::db3 | lcd_pins::en));

  app::util::delay (app::util::miliseconds (7U));

  // B:
  gpio.output_val (lcd_pins::rs | lcd_pins::db6 | lcd_pins::db1 | lcd_pins::en);
  app::util::delay (app::util::miliseconds (7U));
  gpio.output_val (-(lcd_pins::rs | lcd_pins::db6 | lcd_pins::db1 | lcd_pins::en));

  app::util::delay (app::util::miliseconds (7U));

  // D:
  gpio.output_val (lcd_pins::rs | lcd_pins::db6 | lcd_pins::db2 | lcd_pins::en);
  app::util::delay (app::util::miliseconds (7U));
  gpio.output_val (-(lcd_pins::rs | lcd_pins::db6 | lcd_pins::db2 | lcd_pins::en));

  gpio.output_en (-(lcd_pins::rs | lcd_pins::db7 | lcd_pins::db6 | lcd_pins::db5 | lcd_pins::db4 | lcd_pins::db3 | lcd_pins::db2 | lcd_pins::db1 | lcd_pins::db0 | lcd_pins::en));

  std::printf ("%ld", gpio.get_output_en ());
  
  return 0;
}
